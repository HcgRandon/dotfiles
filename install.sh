#!/usr/bin/env bash

set -e
ask() {
  while true; do
    if [ "${2:-}" = "Y" ]; then
      prompt="Y/n"
      default=Y
    elif [ "${2:-}" = "N" ]; then
      prompt="y/N"
      default=N
    else
      prompt="y/n"
      default=
    fi
    read -p "$1 [$prompt] " REPLY </dev/tty
    if [ -z "$REPLY" ]; then
      REPLY=$default
    fi
    case "$REPLY" in
      Y*|y*) return 0 ;;
      N*|n*) return 1 ;;
    esac
  done
}

dir=`pwd`

if ask "Install packages (ubuntu)?" Y; then
 sudo apt-get update && sudo apt-get install i3 i3blocks compton zsh curl arandr feh
fi

if ask "Install antigen.sh" Y; then
 rm -rf ${HOME}/\.antigen.zsh
 curl -L git.io/antigen > ${HOME}/\.antigen.zsh
fi

if ask "Install symlink for zsh?" Y; then
 rm -rf ${HOME}/\.zshrc ${HOME}/\.aliases.sh
 ln -sf ${dir}/zshrc ${HOME}/.zshrc
 ln -sf ${dir}/aliases.sh ${HOME}/.aliases.sh
fi

if ask "Install symlink for .gitconfig?" Y; then
 rm -rf ${HOME}/\.gitconfig
 ln -sf ${dir}/gitconfig ${HOME}/\.gitconfig
fi

if ask "Install symlink for i3?" Y; then
 rm -rf ${HOME}/\.config/i3
 ln -sfn ${dir}/i3 ${HOME}/\.config/i3
fi

if ask "Install symlink for compton cfg?" Y; then
 rm -rf ${HOME}/\.compton.conf
 ln -sfn ${dir}/compton.conf ${HOME}/\.compton.conf
fi

if ask "Install symlink for fonts?" Y; then
 rm -rf ${HOME}/\.fonts
 ln -sf ${dir}/fonts ${HOME}/\.fonts
fi

if ask "Install symlink for gtk?" Y; then
 rm -rf ${HOME}/.gtkrc-2\.0 ${HOME}/.config/gtk-3\.0
 ln -sf ${dir}/gtk/gtkrc-2\.0 ${HOME}/gtkrc-2\.0
 ln -sf ${dir}/gtk/gtk-3\.0 ${HOME}/.config/gtk-3\.0
fi
